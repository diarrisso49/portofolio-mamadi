<?php

use App\Http\Controllers\Admin\CategoriesController;
use App\Http\Controllers\Admin\DashboardController;
use App\Http\Controllers\Admin\EducationController;
use App\Http\Controllers\Admin\ExperienceController;
use App\Http\Controllers\Admin\ProfileController;
use App\Http\Controllers\Admin\ProjectController;
use App\Http\Controllers\Admin\ReposController;
use App\Http\Controllers\Admin\ServiceController;
use App\Http\Controllers\Admin\SkillController;
use App\Http\Controllers\Admin\TechnoController;
use App\Http\Controllers\Admin\UsersController;
use App\Http\Controllers\Auth\AuthenticatedSessionController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\ImagesController;
use App\Http\Resources\EducationsCollection;
use App\Http\Resources\ExperienceCollection;
use App\Models\Education;
use App\Models\Experience;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Auth
Route::get('login', [AuthenticatedSessionController::class, 'create'])->name('login')->middleware('guest');
Route::post('login', [AuthenticatedSessionController::class, 'store'])->name('login.store')->middleware('guest');
Route::delete('logout', [AuthenticatedSessionController::class, 'destroy'])->name('logout');
// Admin

Route::prefix('admin')->group(function () {
    // Dashboard
    Route::get('dashboard', [DashboardController::class, 'index'])->name('dashboard-admin')->middleware('auth');
    // Users
    Route::get('/users', [UsersController::class, 'index'])->name('users')->middleware('auth');
    Route::get('/users/create', [UsersController::class, 'create'])->name('users.create')->middleware('auth');
    Route::post('/users', [UsersController::class, 'store'])->name('users.store')->middleware('auth');
    Route::get('/users/{user}/edit', [UsersController::class, 'edit'])->name('users.edit')->middleware('auth');
    Route::put('/users/{user}', [UsersController::class, 'update'])->name('users.update')->middleware('auth');
    Route::delete('/users/{user}', [UsersController::class, 'destroy'])->name('users.destroy')->middleware('auth');
    Route::put('/users/{user}/restore', [UsersController::class, 'restore'])->name('users.restore')->middleware('auth');

    // Projects
    Route::get('/projects', [ProjectController::class, 'index'])->name('projects')->middleware('auth');
    Route::get('/projects/create', [ProjectController::class, 'create'])->name('projects.create')->middleware('auth');
    Route::post('/projects', [ProjectController::class, 'store'])->name('projects.store')->middleware('auth');
    Route::get('/projects/{project}/edit', [ProjectController::class, 'edit'])->name('projects.edit')->middleware('auth');
    Route::put('/projects/{project}', [ProjectController::class, 'update'])->name('projects.update')->middleware('auth');
    Route::delete('/projects/{project}', [ProjectController::class, 'destroy'])->name('projects.destroy')->middleware('auth');
    Route::put('/projects/{organization}/restore', [ProjectController::class, 'restore'])->name('projects.restore')->middleware('auth');
    Route::post('/projects/{project}/technologies', [ProjectController::class, 'addTechnologies'])->name('projects.technologies')->middleware('auth');

    // skills
    Route::get('/skills', [SkillController::class, 'index'])->name('skills')->middleware('auth');
    Route::get('skills/create', [SkillController::class, 'create'])->name('skills.create')->middleware('auth');
    Route::post('skills', [SkillController::class, 'store'])->name('skills.store')->middleware('auth');
    Route::get('skills/{skill}/edit', [SkillController::class, 'edit'])->name('skills.edit')->middleware('auth');
    Route::put('skills/{skill}', [SkillController::class, 'update'])->name('skills.update')->middleware('auth');
    Route::delete('skills/{skill}', [SkillController::class, 'destroy'])->name('skills.destroy')->middleware('auth');
    Route::put('skills/{skill}/restore', [SkillController::class, 'restore'])->name('skills.restore')->middleware('auth');

    // Repositories
    Route::get('repositories', [ReposController::class, 'index'])->name('repositories')->middleware('auth');
    Route::get('repositories/create', [ReposController::class, 'create'])->name('repositories.create')->middleware('auth');
    Route::post('repositories', [ReposController::class, 'store'])->name('repositories.store')->middleware('auth');
    Route::get('repositories/{repository}/edit', [ReposController::class, 'edit'])->name('repositories.edit')->middleware('auth');
    Route::put('repositories/{repository}', [ReposController::class, 'update'])->name('repositories.update')->middleware('auth');
    Route::delete('repositories/{repository}', [ReposController::class, 'destroy'])->name('repositories.destroy')->middleware('auth');
    Route::put('repositories/{repository}/restore', [ReposController::class, 'restore'])->name('repositories.restore')->middleware('auth');

    // Technologies
    Route::get('technologies', [TechnoController::class, 'index'])->name('technologies')->middleware('auth');
    Route::get('/technologies/create', [TechnoController::class, 'create'])->name('technologies.create')->middleware('auth');
    Route::post('/technologies', [TechnoController::class, 'store'])->name('technologies.store')->middleware('auth');
    Route::get('/technologies/{techo}/edit', [TechnoController::class, 'edit'])->name('technologies.edit')->middleware('auth');
    Route::put('/technologies/{techo}', [TechnoController::class, 'update'])->name('technologies.update')->middleware('auth');
    Route::delete('/technologies/{techo}', [TechnoController::class, 'destroy'])->name('technologies.destroy')->middleware('auth');
    Route::put('technologies/{techo}/restore', [TechnoController::class, 'restore'])->name('technologies.restore')->middleware('auth');

    // Categories
    Route::get('/categories', [CategoriesController::class, 'index'])->name('categories')->middleware('auth');
    Route::get('categories/create', [CategoriesController::class, 'create'])->name('categories.create')->middleware('auth');
    Route::post('categories', [CategoriesController::class, 'store'])->name('categories.store')->middleware('auth');
    Route::get('categories/{category}/edit', [CategoriesController::class, 'edit'])->name('categories.edit')->middleware('auth');
    Route::put('categories/{category}', [CategoriesController::class, 'update'])->name('categories.update')->middleware('auth');
    Route::delete('categories/{category}', [CategoriesController::class, 'destroy'])->name('categories.destroy')->middleware('auth');
    Route::put('categories/{category}/restore', [CategoriesController::class, 'restore'])->name('categories.restore')->middleware('auth');

    // services
    Route::get('services', [ServiceController::class, 'index'])->name('services')->middleware('auth');
    Route::get('services/create', [ServiceController::class, 'create'])->name('services.create')->middleware('auth');
    Route::post('services', [ServiceController::class, 'store'])->name('services.store')->middleware('auth');
    Route::get('services/{service}/edit', [ServiceController::class, 'edit'])->name('services.edit')->middleware('auth');
    Route::put('services/{service}', [ServiceController::class, 'update'])->name('services.update')->middleware('auth');
    Route::delete('services/{service}', [ServiceController::class, 'destroy'])->name('services.destroy')->middleware('auth');
    Route::put('services/{service}/restore', [ServiceController::class, 'restore'])->name('services.restore')->middleware('auth');

    // Educations
    Route::get('educations', [EducationController::class, 'index'])->name('educations')->middleware('auth');
    Route::get('educations/create', [EducationController::class, 'create'])->name('educations.create')->middleware('auth');
    Route::post('educations', [EducationController::class, 'store'])->name('educations.store')->middleware('auth');
    Route::get('educations/{education}/edit', [EducationController::class, 'edit'])->name('educations.edit')->middleware('auth');
    Route::put('educations/{education}', [EducationController::class, 'update'])->name('educations.update')->middleware('auth');
    Route::delete('educations/{education}', [EducationController::class, 'destroy'])->name('educations.destroy')->middleware('auth');
    Route::put('educations/{education}/restore', [EducationController::class, 'restore'])->name('educations.restore')->middleware('auth');

    // Experience
    Route::get('experiences', [ExperienceController::class, 'index'])->name('experiences')->middleware('auth');
    Route::get('experiences/create', [ExperienceController::class, 'create'])->name('experiences.create')->middleware('auth');
    Route::post('experiences', [ExperienceController::class, 'store'])->name('experiences.store')->middleware('auth');
    Route::get('experiences/{experience}/edit', [ExperienceController::class, 'edit'])->name('experiences.edit')->middleware('auth');
    Route::put('experiences/{experience}', [ExperienceController::class, 'update'])->name('experiences.update')->middleware('auth');
    Route::delete('experiences/{experience}', [ExperienceController::class, 'destroy'])->name('experiences.destroy')->middleware('auth');
    Route::put('experiences/{experience}/restore', [ExperienceController::class, 'restore'])->name('experiences.restore')->middleware('auth');

    // Images
    Route::get('/img/{path}', [ImagesController::class, 'show'])
        ->where('path', '.*')
        ->name('image');

    // Profile
    Route::get('/profile', [ProfileController::class, 'index'])->name('profile')->middleware('auth');
    Route::get('/profile/edit', [ProfileController::class, 'edit'])->name('profile.edit')->middleware('auth');
    Route::put('/profile', [ProfileController::class, 'update'])->name('profile.update')->middleware('auth');
    Route::put('/profile/password', [ProfileController::class, 'password'])->name('profile.password')->middleware('auth');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy')->middleware('auth');

});

//Frontend
Route::get('/', [HomeController::class, 'index'])->name('home');
Route::post('/contact', [HomeController::class, 'store'])->name('contact');

Route::get('/resources/education', function () {
    return new EducationsCollection(Education::all());
});

Route::get('/resources/experience', function () {
    return new ExperienceCollection(Experience::all());
});
Route::get('/about', function () {
    return Inertia::render('Frontend/About/Index');
})->name('about');
