import '../css/app.css'

import {createApp, h} from 'vue'
import {createInertiaApp} from '@inertiajs/vue3'
import {FontAwesomeIcon} from './font-awesome';

createInertiaApp({
    resolve: name => {
        const pages = import.meta.glob('./Pages/**/*.vue', {eager: true})

        if (!pages[`./Pages/${name}.vue`]) {
            throw new Error(`La page ${name} n'existe pas ou n'a pas été correctement importée.`);
        }
        return pages[`./Pages/${name}.vue`]
    },
    title: title => title ? `${title} - Mamadi Diarrisso` : 'Mamadi Diarrisso',
    setup({el, App, props, plugin}) {
        createApp({render: () => h(App, props)})
            .use(plugin)
            .component('font-awesome-icon', FontAwesomeIcon)
            .mount(el)
    },
})
