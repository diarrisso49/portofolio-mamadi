// resources/js/font-awesome.js
import {library} from '@fortawesome/fontawesome-svg-core';
import {FontAwesomeIcon} from '@fortawesome/vue-fontawesome';
import {
    faCss3Alt,
    faDrupal,
    faHtml5,
    faJs,
    faPhp,
    faReact,
    faVuejs,
    faWordpress
} from '@fortawesome/free-brands-svg-icons';

// Ajouter les icônes nécessaires à la bibliothèque
library.add(faHtml5, faCss3Alt, faPhp, faJs, faVuejs, faReact, faDrupal, faWordpress);

export {FontAwesomeIcon};
