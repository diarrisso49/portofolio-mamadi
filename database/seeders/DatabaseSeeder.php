<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use App\Models\Account;
use App\Models\Contact;
use App\Models\Organization;
use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;


class DatabaseSeeder extends Seeder
{
    use WithoutModelEvents;
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        // \App\Models\User::factory(10)->create();

        // \App\Models\User::factory()->create([
        //     'name' => 'Test User',
        //     'email' => 'test@example.com',
        // ]);
   /*     $this->call([
            UserSeeder::class,
        ]);*/



        User::factory()->create([
            'name' => 'Masingacite Diarrisso',
            'first_name' => 'Mamadi',
            'last_name' => 'diarrisso',
            'email' => 'diarrisso49@gmail.com',
            'password' => Hash::make('Matsinga76'),
        ]);


    }
}
