<?php

namespace Database\Seeders;

use App\Models\Techo;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class TechoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
         Techo::factory()->count(10)->create();
    }
}
