<?php

namespace Database\Seeders;

use App\Models\Repos;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ReposSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Repos::factory()->count(10)->create();
    }
}
