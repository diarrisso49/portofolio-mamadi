<?php

namespace Database\Factories;

use App\Models\Categorie;
use App\Models\Skills;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends Factory<Skills>
 */
class SkillsFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'name' => $this->faker->unique()->word,
            'level' => $this->faker->numberBetween(1, 10),
            'experiance' => $this->faker->numberBetween(1, 10),
            'category_id' =>  Categorie::factory(),
        ];
    }
}
