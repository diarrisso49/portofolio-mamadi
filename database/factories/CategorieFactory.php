<?php

namespace Database\Factories;

use App\Models\Categorie;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends Factory<Categorie>
 */
class CategorieFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'name' => $this->faker->randomElement(['E-commerce', 'static Web', 'Web-Application'. 'Application-Mobile', 'Customer', 'Private', 'Training', 'Blog']),
            'type' => $this->faker->randomElement(['Laravel', 'wordpress', 'Symfony', 'React', 'Vuejs', 'Flutter',' PHP', 'JS'])
        ];
    }
}
