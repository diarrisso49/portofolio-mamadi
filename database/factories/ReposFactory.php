<?php

namespace Database\Factories;

use App\Models\Repos;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends Factory<Repos>
 */
class ReposFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'project_id' => \App\Models\Project::all()->random()->id,
            'user_id' => \App\Models\User::all()->random()->id,
            'link' => $this->faker->url,
        ];
    }
}
