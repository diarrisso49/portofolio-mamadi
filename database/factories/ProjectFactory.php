<?php

namespace Database\Factories;

use App\Models\Categorie;
use App\Models\Project;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends Factory<Project>
 */
class ProjectFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'name' => $this->faker->sentence(3),
            'user_id' => User::factory(),
            'category_id' => Categorie::factory(),
            'description' => $this->faker->paragraph(3),
            'link' => $this->faker->url,
            'image' => $this->faker->imageUrl(640, 480, 'technics'),
        ];
    }
}
