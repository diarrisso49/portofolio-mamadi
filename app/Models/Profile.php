<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    use HasFactory;
protected $table = 'profile';
    protected $fillable = [
        'user_id',
        'titre',
        'email',
        'phone',
        'city',
        'zip',
        'street',
        'zip',
        'country',
        'description',
        'photo_profile',
    ];
}
