<?php

namespace App\Models;

use App\Http\Enums\CategorieType;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Categorie extends Model
{
    use HasFactory;


    protected $fillable = [
        'name',
        'type',

    ];


    public function projects(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(Project::class);
    }

    public function setTypeAttribute($value): void
    {
        $this->attributes['type'] = in_array($value, [CategorieType::PERSONAL, CategorieType::PROFESSIONAL]) ? $value : null;
    }
}
