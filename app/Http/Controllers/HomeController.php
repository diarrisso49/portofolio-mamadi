<?php

namespace App\Http\Controllers;

use App\Mail\ContactMail;
use App\Models\Project;
use App\Models\Service;
use App\Models\Skill;
use Illuminate\Support\Facades\Mail;
use Inertia\Inertia;
use Inertia\Response;

class HomeController extends Controller
{
    public function index(): Response
    {
        $services = Service::all();
        $skills = Skill::all();
        $projects = Project::with('category', 'user', 'technologies')->get();
        return Inertia::render('Frontend/Home/Index', [
            'services' => $services,
            'skills' => $skills,
            'projects' => $projects,
        ]);
    }

    public function store()
    {
        request()->validate([
            'name' => 'required',
            'email' => 'required|email',
            'message' => 'required',
        ]);

        $data = request()->all();

        Mail::to('diarrisso49@gmail.com')->send(new ContactMail($data));

    }
}
