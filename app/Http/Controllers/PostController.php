<?php

namespace App\Http\Controllers;

use App\Http\Resources\EducationCollection;
use App\Http\Resources\PostCollection;
use App\Models\Post;
use App\Models\User;
use Exception;
use Inertia\Response;
use Inertia\ResponseFactory;

class PostController extends Controller
{
    /**
     * @throws Exception
     */
    public function index(): Response|ResponseFactory
    {
        $user = User::find(22);
        $user->assignRole('admin');
        $posts = EducationCollection::collection(Post::all());
        $posts = new PostCollection(Post::all());
        $posts = $posts->response()->getData();
        $posts = Post::all();
        return inertia('Post/Index', compact('posts'));
    }
}
