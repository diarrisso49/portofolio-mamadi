<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Skill;
use App\Models\User;
use Illuminate\Http\Request;
use Inertia\Inertia;

class SkillController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(): \Inertia\Response
    {
        return Inertia::render('Admin/Skills/Index', [
            'skills' =>Skill::all(),

        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return Inertia::render('Admin/Skills/Create', [
            'users' => User::all(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Skill $skill, Request $request)
    {
        $request->validate([
            'name' => 'required',
            'experiance' => 'required',
            'user_id' => 'required',
            'level' => 'required',
        ]);

        $user = \Auth::user();

        $skills = Skill::create([
            'name' => $request->name,
            'experiance' => $request->experiance,
            'user_id' => $request->user_id,
            'level' => $request->level,

        ]);

        $user->skills()->attach($skills);

        return redirect()->route('skills')->with('success', 'Skill created.');
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        return Inertia::render('Admin/Skills/Edit', [
            'skill' => Skill::find($id),
            'users' => User::all(),
        ]);
    }
    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {

        $request->validate([
            'name' => 'required',
            'experiance' => 'required',
            'user_id' => 'required',
            'level' => 'required',
        ]);

        $skill = Skill::find($id);

        $skill->update([
            'name' => $request->name,
            'experiance' => $request->experiance,
            'user_id' => $request->user_id,
            'level' => $request->level,
        ]);

        return redirect()->route('skills')->with('success', 'Skill updated.');
    }
    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Skill $skill)
    {
        $skill->delete();

        return redirect()->route('skills')->with('success', 'Skill deleted.');
    }
}
