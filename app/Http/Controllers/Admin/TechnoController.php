<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Project;
use App\Models\Techo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Inertia\Inertia;

class TechnoController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(): \Inertia\Response
    {
        return Inertia::render('Admin/Technologies/Index', [
            'technologies' => Techo::all(),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return Inertia::render('Admin/Technologies/Create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => ['required', 'max:50'],
            'level' => ['required'],
        ]);
        $user = Auth::user();
        $project = Project::find(1);

        $techo = Techo::create([
            'name' => $request->input('name'),
            'level' => $request->input('level'),
        ]);

        if ($user) {
            $techo->users()->attach($user->id);
        }

       $project->technologies()->attach($techo->id);

        return redirect()->route('technologies')->with('success', 'Technology created.');
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id): \Inertia\Response
    {
        return Inertia::render('Admin/Technologies/Edit', [
            'technologies' => Techo::all()->find($id)->getAttributes(),
        ]);
    }


    /**
     * Update the specified resource in storage.
     */
    public function update(Techo $techo, Request $request)
    {
    $request->validate([
            'name' => ['required'],
            'level' => ['required'],
        ]);

        $user = Auth::user();

        $techo->update([
            'name' => $request->input('name'),
            'level' => $request->input('level'),
        ]);

        if ($user) {
            $techo->users()->sync([$user->id]);
        }

        return redirect()->route('technologies')->with('success', 'Technology updated.');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Techo $techo)
    {
        $techo->delete();

        return redirect()->route('technologies')->with('success', 'Technology deleted.');
    }

}
