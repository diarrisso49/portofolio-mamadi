<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Categorie;
use App\Models\Project;
use App\Models\Techo;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;
use Inertia\Inertia;

class ProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $projects = Project::with('category', 'user')->get();
        return Inertia::render('Admin/Projects/Index', [
            'projects' => Project::with('category', 'user')->get()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(): \Inertia\Response
    {
        return Inertia::render('Admin/Projects/Create', [
            'categories' => Categorie::all(),
            'users' => User::all(),
            'technologies' => Techo::all(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {


        $validated = $request->validate([
            'name' => ['required', 'max:50'],
            'description' => ['required', 'max:50'],
            'link' => ['required'],
            'category_id' => ['required'],
            'user_id' => ['required'],
            'image' => ['nullable', 'image'],


       ]);

        $project = Project::create($validated);

        if ($request->input('technologies')) {
            $project->technologies()->sync($request->technologies);
        }

        return redirect()->route('projects')->with('success', 'Project created.');
    }


    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        return Inertia::render('Admin/Projects/Edit', [
            'projects' => Project::with('category', 'user')->find($id),
            'categories' => Categorie::all(),
            'users' => User::all(),
        ]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Project $project, Request $request): \Illuminate\Http\RedirectResponse
    {
        $request->validate([
            'name' => ['required', 'max:50'],
            'description' => ['required', 'max:50'],
            'link' => ['required'],
            'category_id' => ['required'],
            'user_id' => ['required'],
            'image' => ['nullable', 'image'],
        ]);

        $project->update([
            'name' => $request->input('name'),
            'description' => $request->input('description'),
            'link' => $request->input('link'),
            'category_id' => $request->input('category_id'),
            'user_id' => $request->input('user_id'),
            'image' => $request->file('image') ? $request->file('image')->store('projects') : null,
        ]);

        return redirect()->route('projects')->with('success', 'Project updated.');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Project  $project): \Illuminate\Http\RedirectResponse
    {
        $project->delete();
        return redirect()->route('projects')->with('success', 'Project deleted.');
    }

    public function addTechnologies(Request $request, Project $project): \Illuminate\Http\JsonResponse
    {
        $request->validate([
            'technology_ids' => 'required|array',
            'technology_ids.*' => 'exists:technologies,id'
        ]);

        $project->technologies()->attach($request->technology_ids);

        return response()->json(['message' => 'Technologies added successfully'], 200);
    }

}
