<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Enums\CategorieType;
use App\Models\Categorie;
use Illuminate\Http\Request;
use Inertia\Inertia;

class CategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(): \Inertia\Response
    {
        return Inertia::render('Admin/Categories/Index', [
            'categories' => Categorie::all(),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return Inertia::render('Admin/Categories/Create', [
        ]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request, Categorie $categorie)
    {
        $request->validate([
            'name' => 'required',
            'type' => ['required', function ($attribute, $value, $fail) {
                if (! in_array($value, [CategorieType::PERSONAL, CategorieType::PROFESSIONAL])) {
                    $fail('The selected category type is invalid.');
                }
            }],
        ]);

        $categorie->create([
            'name' => $request->get('name'),
            'type' => $request->get('type'),
        ]);

        return redirect()->route('categories')->with('success', 'Category created successfully.');
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Categorie $category)
    {
        return Inertia::render('Admin/Categories/Edit', [
            'categories' => $category,
        ]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Categorie $category, Request $request): \Illuminate\Http\RedirectResponse
    {
        $request->validate([
            'name' => 'required',
            'type' => ['required', function ($attribute, $value, $fail) {
                if (! in_array($value, [CategorieType::PERSONAL, CategorieType::PROFESSIONAL])) {
                    $fail('The selected category type is invalid.');
                }
            }],
        ]);

        $category->update([
            'name' => $request->get('name'),
            'type' => $request->get('type'),
        ]);

        return redirect()->route('categories')->with('success', 'Category updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Categorie $category): \Illuminate\Http\RedirectResponse
    {

        $category->delete();

        return redirect()->route('categories')->with('success', 'Category deleted successfully.');
    }
}
