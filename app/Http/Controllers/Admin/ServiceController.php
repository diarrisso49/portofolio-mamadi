<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Inertia\Inertia;
use App\Models\Service;

class ServiceController extends Controller
{

     /**
      * Display a listing of the resource.
      */
     public function index(): \Inertia\Response
     {
        return Inertia::render('Admin/Service/Index', [
            'services' => Service::all()
        ]);
     }

     /**
      * Show the form for creating a new resource.
      */
     public function create(): \Inertia\Response
     {
         return Inertia::render('Admin/Service/Create');
     }

     /**
      * Store a newly created resource in storage.
      */
     public function store(Request $request): \Illuminate\Http\RedirectResponse
     {
        request()->validate([
            'name' => 'required',
            'description' => 'required',

        ]);

        Service::create($request->all());

        return redirect()->route('services')->with('success', 'Service created successfully.');

     }



     /**
      * Show the form for editing the specified resource.
      */
     public function edit(Service $service): \Inertia\Response
     {

         return Inertia::render('Admin/Service/Edit', [
             'services' => $service
         ]);
     }

     /**
      * Update the specified resource in storage.
      */
     public function update(Service $service, Request $request): \Illuminate\Http\RedirectResponse
     {
        request()->validate([
            'name' => 'required',
            'description' => 'required',

        ]);

        $service->update([
            'name' => $request->name,
            'description' => $request->description,
        ]);

        return redirect()->route('services')->with('success', 'Service updated successfully.');
     }

     /**
      * Remove the specified resource from storage.
      */
     public function destroy(Service $service)
     {
         $service->delete();

         return redirect()->route('services')->with('success', 'Service deleted successfully.');
     }

}

