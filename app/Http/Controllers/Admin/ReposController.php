<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Project;
use App\Models\Repos;
use App\Models\User;
use Illuminate\Http\Request;
use Inertia\Inertia;

class ReposController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(): \Inertia\Response
    {
        return Inertia::render('Admin/GitRepository/Index', [
            'repositories' =>  Repos::with('project', 'user')->get()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(): \Inertia\Response
    {
        return Inertia::render('Admin/GitRepository/Create',[
            'projects' => Project::all(),
            'users' => User::all(),
        ]);

    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request, Repos $repository)
    {
        $request->validate([
            'project_id' => 'required',
            'user_id' => 'required',
            'link' => 'required',
        ]);

        $repository->create([
            'project_id' => $request->get('project_id'),
            'user_id' => $request->get('user_id'),
            'link' => $request->get('link'),
        ]);

        return redirect()->route('repositories')->with('success', 'GitRepository created successfully.');
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Repos $repository, Request $request): \Inertia\Response
    {
        return Inertia::render('GitRepository/Edit',[
            'projects' => Project::all(),
            'users' => User::all(),
            'repositories' => $repository,
        ]);
    }
    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Repos $repository ): \Illuminate\Http\RedirectResponse
    {
        $request->validate([
            'project_id' => 'required',
            'user_id' => 'required',
            'link' => 'required',
        ]);

        $repository->update([
            'project_id' => $request->get('project_id'),
            'user_id' => $request->get('user_id'),
            'link' => $request->get('link'),
        ]);

        return redirect()->route('repositories')->with('success', 'GitRepository updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Repos $repository): \Illuminate\Http\RedirectResponse
    {
        $repository->delete();

        return redirect()->route('repositories')->with('success', 'GitRepository deleted successfully.');
    }

}
