<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\URL;
use Illuminate\Validation\Rule;
use Inertia\Inertia;
use Illuminate\Http\Request;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(): \Inertia\Response
    {
         return Inertia::render('Admin/Users/Index', [
          'users' => User::all(),

    ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(): \Inertia\Response
    {
        return Inertia::render('Admin/Users/Create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request): \Illuminate\Http\RedirectResponse
    {
        $request->validate([
        'first_name' => ['required', 'max:50'],
        'last_name' => ['required', 'max:50'],
        'email' => ['required', 'max:50', 'email', Rule::unique('users')],
        'password' => ['nullable'],
        'owner' => ['required', 'boolean'],
        'photo' => ['nullable', 'image'],
    ]);
         /*User::create([
                'first_name' =>  Request::get('first_name'),
                'name' =>  Request::get('first_name') . ' ' .  Request::get('last_name') ,
                'last_name' => Request::get('last_name'),
                'email' =>  Request::get('email'),
                'password' =>  Request::get('password'),
                'profile_photo_path' =>  Request::file('photo') ? Request::file('photo')->store('users') : null,
         ]);*/

        User::create([
            'first_name' => $request->input('first_name'),
            'name' => $request->input('first_name') . ' ' . $request->input('last_name'),
            'last_name' => $request->input('last_name'),
            'email' => $request->input('email'),
            'password' => $request->input('password') ? bcrypt($request->input('password')) : null,
            'profile_photo_path' => $request->file('photo') ? $request->file('photo')->store('users') : null,
        ]);

         return redirect()->route('users')->with('success', 'User created successfully.');
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(User $user): \Inertia\Response
    {
        return Inertia::render('Admin/Users/Edit', [
            'user' => [
                'id' => $user->id,
                'name' => $user->first_name . ' ' . $user->last_name,
                'first_name' => $user->first_name,
                'last_name' => $user->last_name,
                'email' => $user->email,
                'profile_photo_path' => $user->photo_path ? URL::route('image', ['path' => $user->photo_path, 'w' => 60, 'h' => 60, 'fit' => 'crop']) : null,
                'deleted_at' => $user->deleted_at,
            ],
        ]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(User $user, Request $request): \Illuminate\Http\RedirectResponse
    {
        \Illuminate\Support\Facades\Request::validate([
            'first_name' => ['required', 'max:50'],
            'last_name' => ['required', 'max:50'],
            'email' => ['required', 'max:50', 'email', Rule::unique('users')->ignore($user->id)],
            'password' => ['nullable'],
            'photo' => ['nullable', 'image'],
        ]);

        $user->update([
            'first_name' => $request->input('first_name'),
            'name' => $request->input('first_name') . ' ' . $request->input('last_name'),
            'last_name' => $request->input('last_name'),
            'email' => $request->input('email'),
            'password' => $request->input('password') ? bcrypt($request->input('password')) : null,

        ]);

        if ($request->file('profile_photo_path')) {
            $user->update(['profile_photo_path' => $request->file('photo')->store('users')]);
        }

        if ($request->get('password')) {
            $user->update(['password' => $request->get('password')]);
        }

        return redirect()->route('users')->with('success', 'User updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(User $user)
    {
        $user->delete();

       return redirect()->route('users')->with('success', 'User deleted successfully.');
    }

    public function restore(User $user): RedirectResponse
    {
        $user->restore();

       return redirect()->route('users')->with('success', 'User restored successfully.');
    }
}


