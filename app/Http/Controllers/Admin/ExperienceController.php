<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Experience;
use Illuminate\Http\Request;
use Inertia\Inertia;

class ExperienceController extends Controller
{

    public function index()
    {
        return Inertia::render('Admin/Experience/Index', [
            'experiences' => Experience::all(),
        ]);
    }

    public function create()
    {
        return Inertia::render('Admin/Experience/Create');
    }


    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'start_year' => 'required|date',
            'end_year' => 'required|date',
            'place' => 'required',
            'description' => 'required',
        ]);

        Experience::create($request->all());

        return redirect()->route('experiences')->with('success', 'Experience created.');
    }

    public function edit(Experience $experience): \Inertia\Response
    {
        return Inertia::render('Admin/Experience/Edit', [
            'experiences' => $experience,
        ]);
    }

    public function update(Request $request, Experience $experience): \Illuminate\Http\RedirectResponse
    {
        $request->validate([
            'name' => 'required',
            'start_year' => 'required|date',
            'end_year' => 'required|date',
            'place' => 'required',
            'description' => 'required',
        ]);

        $experience->update([
            'name' => $request->name,
            'start_year' => $request->start_year,
            'end_year' => $request->end_year,
            'place' => $request->place,
            'description' => $request->description,

        ]);

        return redirect()->route('experiences')->with('success', 'Experience updated.');
    }


    public function destroy(Experience $experience): \Illuminate\Http\RedirectResponse
    {
        $experience->delete();

        return redirect()->route('experiences')->with('success', 'Experience deleted.');
    }
}
