<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Education;
use Illuminate\Http\Request;
use Inertia\Inertia;

class EducationController extends Controller
{
    public function index(): \Inertia\Response
    {
        return Inertia::render('Admin/Education/Index', [
            'educations' => Education::all(),
        ]);
    }

    public function create(): \Inertia\Response
    {
        return Inertia::render('Admin/Education/Create');
    }

    public function store(Request $request): \Illuminate\Http\RedirectResponse
    {
        request()->validate([
            'name' => 'required',
            'description' => 'required',
            'start_year' => 'required|date',
            'end_year' => 'required|date',
            'place' => 'required',

        ]);

        Education::create($request->all());

        return redirect()->route('educations')->with('success', 'Education created successfully.');

    }

    public function edit(Education $education): \Inertia\Response
    {

        return Inertia::render('Admin/Education/Edit', [
            'educations' => $education,
        ]);
    }

    public function update(Request $request, Education $education): \Illuminate\Http\RedirectResponse
    {
        request()->validate([
            'name' => 'required',
            'description' => 'required',
            'start_year' => 'required|date',
            'end_year' => 'required|date',
            'place' => 'required',
        ]);

        $education->update([
            'name' => $request->name,
            'description' => $request->description,
            'start_year' => $request->start_year,
            'end_year' => $request->end_year,
            'place' => $request->place,
        ]);

        return redirect()->route('educations')->with('success', 'Education updated successfully.');
    }

    public function destroy(Education $education): \Illuminate\Http\RedirectResponse
    {
        $education->delete();

        return redirect()->route('educations')->with('success', 'Education deleted successfully.');

    }
}
