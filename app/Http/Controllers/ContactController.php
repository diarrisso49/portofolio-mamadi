<?php

namespace App\Http\Controllers;

use App\Models\Project;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Inertia\Inertia;

class ContactController extends Controller
{


    public function create(): \Inertia\Response
    {
        return Inertia::render('',[

        ]);

    }
    public function store(Request $request):void
    {
        request()->validate([
            'name' => 'required',
            'email' => 'required|email',
            'message' => 'required',
            'subject' => 'required'
        ]);

        $data = request()->all();

        Mail::to('diarrisso49@gmail.com')->send(new \App\Mail\ContactMail($data));
    }
}
