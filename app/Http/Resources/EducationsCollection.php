<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\ResourceCollection;

class EducationsCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @return array<int|string, mixed>
     */
    public function toArray(Request $request): array
    {

        $educations = [];
        foreach ($this->collection as $education) {
            $key = $education->start_year.'-'.$education->end_year;
            $customDate = explode('-', $key);
            $key = $customDate[0].'-'.$customDate[3];
            $educations[$key] = [
                'name' => $education->name,
                'description' => $education->description,
                'start_year' => $education->start_year,
                'end_year' => $education->end_year,
            ];

        }

        return $educations;

    }
}
