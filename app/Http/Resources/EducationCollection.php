<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\ResourceCollection;

class EducationCollection extends ResourceCollection
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        $education = [];
        foreach ($this->collection as $education) {
            $key = $education->start_year . '-' . $education->end_year;
            $education[$key] = [
                'name' => $education->name,
                'description' => $education->description,
                'start_year' => $education->start_year,
                'end_year' => $education->end_year,
            ];
        }

        return $education;
    }
}
