import defaultTheme from 'tailwindcss/defaultTheme';
import forms from '@tailwindcss/forms';
import typography from '@tailwindcss/typography';

/** @type {import('tailwindcss').Config} */
export default {
    content: [
        './vendor/laravel/framework/src/Illuminate/Pagination/resources/views/*.blade.php',
        './vendor/laravel/jetstream/**/*.blade.php',
        './storage/framework/views/*.php',
        './resources/views/**/*.blade.php',
        './resources/js/**/*.vue',
    ],
    darkMode: "class",
    theme: {
        fontFamily: {
            sora: ["Sora", "sans-serif"],
        },
        container: {
            center: true,
            padding: "1rem",
        },
        extend: {
            colors: {
                transparent: "transparent",
                current: "currentColor",
                purple: "#994FF5",
                warning: "#FFC41F",
                light: "#F8F7F6",
                dark: "#050C17",
                gray: "#7780A1",
                white: "#FFFFFF",
            },
            backgroundImage: {
                "gradient-radial": "radial-gradient(var(--tw-gradient-stops))",
                "gradient-liner": "linear-gradient(var(--tw-gradient-stops))",
            },
        },
    },

    plugins: [require("@tailwindcss/forms")],
};


