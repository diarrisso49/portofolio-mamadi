# Laravel + Vue.js + Inertia.js Project

This project uses Laravel for the backend, Vue.js for the frontend, and Inertia.js to facilitate communication between
the two. We use DDEV for the local development environment and Vite as the bundler for frontend assets.

---

## Prerequisites

- **PHP 8.0 ou supérieur**
- **Node.js** (avec npm ou yarn)
- **Docker** et **DDEV** installés

---

## Installation

Suivez ces étapes pour configurer le projet localement :

1. **Cloner le dépôt :**

   ```bash
   git clone https://github.com/your-username/your-project.git
   cd your-project


2. **Install PHP dependencies:**

   ```bash
     composer install

3. **Configure the environment:**

   ```bash 
   Copy the .env.example file to .env and configure the necessary environment variables, especially for the database.

4. **Start DDEV:**

   ```bash
     ddev start

5. **Install JavaScript dependencies:**

   ```bash
   npm install

6. **Generate the Laravel application key:**

   ```bash
     php artisan key:generate

7. **Start the development server:**

   ```bash
    npm run dev

**This will start Vite and allow you to see real-time changes**

## Usage

- Access the application:

  <p>Once the development server is running, you can access your application via the URL provided by DDEV,
  typically https://your-project.ddev.site.</p>


- Development:

  <p>You can now start developing your application. Changes made to Vue.js files will automatically reload thanks to Vite.</p>

# Contributing

## If you wish to contribute to this project, please follow these steps:

1. Fork the repository.
2. Create a branch for your feature (git checkout -b feature/new-feature).
3. Commit your changes (git commit -m 'Add new feature').
4. Push to the branch (git push origin feature/new-feature).
5. Open a Pull Request.

## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
